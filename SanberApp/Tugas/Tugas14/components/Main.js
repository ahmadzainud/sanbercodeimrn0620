import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

import Note from './Note';

export default class Main extends React.Component {
  constructor(props){
    super(props);
    this.state={
      noreArray:[],
      notetext:'',
    }
  }
  render() {
    let notes=this.state.noreArray.map((val,key) =>{
      return <Note key={key} keyval={key} val = {val}
            deleMethode={ ()=> this.deleNode(key)}      
      />
    });
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>NoTe</Text>
        </View>
        <ScrollView style={styles.scrollContainer}>
          {notes}
        </ScrollView>
        <View style={styles.footer}>
          <TextInput style={styles.textInput}
          placeholder='Catatan'
          onChangeText={(notetext) => this.setState({notetext})}
          value={this.state.notetext}
          placeholderTextColor='white'
          underlineColorAndroid='transparent'
          >
          </TextInput>
        </View>

        <TouchableOpacity onPress={this.addNote.bind(this)} style={styles.addButton}>
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      </View>
      
    )
  }
  addNote(){
    if(this.state.notetext){
      var d= new Date();
      this.state.noreArray.push({
      'date' :d.getFullYear()+
      "/" +(d.getMonth()+1)+
      "/" +d.getDate(),
      'note':this.state.notetext
      });
      this.setState({ noreArray:this.state.noreArray })
      this.setState({notetext:''})
    }else{
      alert('adnada belum memasuakn note');
    }
  }

  deleNode(key){
    this.state.noreArray.splice(key,1)
    this.setState({ noreArray:this.state.noreArray })
  }
 
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#E91E63',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#E91E63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});

