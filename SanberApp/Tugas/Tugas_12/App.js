import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text,FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'
import Videoitem from './componen/videoitem'
import data from './data.json'

export default class App extends React.Component {
    render(){
       // alert(data.kind);
        return (
            <View style={styles.container}>
                <View style={styles.navbar}>
                <Image source={require('./images/logo.png')} style={{width:98,height:22}} />
                <View style={styles.rightnavbar}>
                    <TouchableOpacity>
                        <Icon style={styles.navitem} name="search" size={25}></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon style={styles.navitem} name="account-circle" size={25}></Icon>
                    </TouchableOpacity>
                </View>
                </View>
            <View style={styles.body}>
               <FlatList 
                data={data.items}
                renderItem={(video)=> <Videoitem video={video.item} />}
                keyExtractor={(item)=> item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                />

                
            </View>
            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="home" size={25}></Icon>
                    <Text style={styles.tabTitel}>Home</Text>

                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="whatshot" size={25}></Icon>
                    <Text style={styles.tabTitel}>Trending</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="subscriptions" size={25}></Icon>
                    <Text style={styles.tabTitel}>Subscribe</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}>
                    <Icon name="folder" size={25}></Icon>
                    <Text style={styles.tabTitel}>Liblary</Text>
                </TouchableOpacity>
            </View>
            </View>
        );
    }
}


const styles=StyleSheet.create({
    container:{
        flex:1,
       

    },
    navbar:{
    height:55,
    backgroundColor:'white',
    elevation:8,
    paddingHorizontal:15,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    paddingTop:20,

    //  borderColor:'black',
    //  borderRadius:
    

    },
    rightnavbar:{
        flexDirection:'row',
    },
    navitem:{
        marginLeft:25
    },
    body:{
        flex:1
    },

    tabBar:{
        backgroundColor:'white',
        height:60,
        borderWidth:0.5,
        borderColor:'#E5E5E5',
        flexDirection:'row',
        justifyContent:'space-around'

    },
    tabItem:{
        alignItems:'center',
        justifyContent:'center'
    },
    tabTitel:{
        fontSize:11,
        color:'#3c3c3c',
        paddingTop:4
    }


});