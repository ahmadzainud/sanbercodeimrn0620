import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Videoitem extends React.Component {
    render(){
        let video=this.props.video;
        //alert(video.id);
        return (
            <View style={styles.container}>
                 <Image source={{uri:video.snippet.thumbnails.medium.url}} 
                 style={{height:200}} />
                 <View style={styles.descContainer}>
                 <Image source={{uri:video.snippet.thumbnails.medium.url}} 
                 style={{height:50,width:50,borderRadius:25}} />
                 <View style={styles.videoDetail}>
                     <Text style={styles.videoTitel}>
                         {video.snippet.title}
                     </Text>
                     <Text style={styles.videostate}>
                         {video.snippet.channelTitle +" . "+ nFormatter(video.statistics.viewCount,1)+" 3 month Ago "}
                     </Text>
                     
                 </View>
                 <TouchableOpacity >
                         <Icon name="more-vert" size={20} color="#999999"></Icon>
                     </TouchableOpacity>
                 </View>

            </View>
        );
    }
}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol+'   View';
  }

const styles=StyleSheet.create({
    container:{
        padding:15
    },
    descContainer:{
        flexDirection:'row',
        paddingTop:15
    },
    videoTitel:{
        fontSize:16,
        color:'#3c3c3c'
    },
    videoDetail:{
        paddingHorizontal:15,
        flex:1
    },
    videostate:{
        fontSize:15,
        paddingTop:3

    }

});