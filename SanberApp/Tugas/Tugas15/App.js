import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer,StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Loginpage from '../Tugas13/LoginScreen'
import about from '../Tugas13/AboutScreen'
import skillscreen from '../Tugas14/SkillScreen'


const Stackroot = createStackNavigator();
const StackDrawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();


function AddScreen({ navigation }) {
  return (
    <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
      <Text >Halaman Tambah</Text></View>
  );
}

function ProjectScreen({ navigation }) {
  return (
    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}><Text>Halaman Proyek</Text></View>
  );
}

function LoginScreen({ navigation }) {
  return (
    <Loginpage>{navigation}</Loginpage>
  );
}


function drawerscreen({ navigation }) {
  console.log('ini drawerscreen ');
  return (
   
      <StackDrawer.Navigator initialRouteName="Home">
        <StackDrawer.Screen name="tabscreen" component={tabscreen} />
        <StackDrawer.Screen name="About" component={about} />
        <StackDrawer.Screen name="Keluar" component={Loginpage} />
      </StackDrawer.Navigator>
   
  );
}

function tabscreen({ navigation }) {
  return (
    <Tab.Navigator>
    <Tab.Screen name="SKILL" component={skillscreen} />
    <Tab.Screen name="Halaman Proyek" component={ProjectScreen} />
    <Tab.Screen name="Halaman Tambah" component={AddScreen} />
  </Tab.Navigator>
  )
}

function aboutscreen({ navigation }) {
  return (
    <View></View>
  )
}

function App() {
  return (
    <NavigationContainer>
      <Stackroot.Navigator initialRouteName="Login" headerMode="none">
        <Stackroot.Screen name="Login" component={Loginpage} />
        <Stackroot.Screen name="drawerscreen" component={drawerscreen} />
      </Stackroot.Navigator>
    </NavigationContainer>
  );
}

export default App;