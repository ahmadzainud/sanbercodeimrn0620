import React from 'react';
import { View,StyleSheet,Image, TouchableOpacity,Text,FlatList,TextInput,Button,Scene,ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'


export default function App( {navigation} ) {
       
    return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.navbaricon}>
                    <Image source={require('./images/logo.png')} style={{width:374,height:110,padding:60}} />
                    </View>
                    <Text style={styles.logincss}>Login</Text>
                    <Text style={styles.textboxcss}>Username / Email</Text>
                    <TextInput style={styles.boxinput}></TextInput>
                    <Text style={styles.textboxcss}>Password</Text>
                    <TextInput style={styles.boxinput}></TextInput>
                    <TouchableOpacity  
                    onPress={() => navigation.navigate('drawerscreen')}
                    >
                    <View style={styles.buttonmasuk}> 
                        <Text style={styles.buttontext}>Masuk</Text>    
                     </View> 
                     </TouchableOpacity>
                     <Text style={styles.ataucss}>atau</Text>
                     <View style={styles.buttondaftar}> 
                        <Text style={styles.buttontext}>Daftar ?</Text>    
                     </View> 
                       
                </View>
            </ScrollView>
        )
}

const styles=StyleSheet.create({
    container:{
        flex:1,
       // backgroundColor:'white',
       

    },
    
    logincss:{
        //position:'absolute',
        width: 88,
        height: 28,
        left: 143,
        margin:20,
        //fontFamily:'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        borderRadius:2,
        borderColor:'#FFFFFF',
        color:'#003366',
    },
    textboxcss:{
        // position:'relative',
         left: 42,
         margin:5,
         justifyContent:'flex-start',
         //fontFamily:'Roboto',
         fontStyle: 'normal',
         fontWeight: 'normal',
         fontSize: 16,
         lineHeight: 19,
         color:'#003366',
     },
     boxinput:{
         width: 294,
         height: 48,
         left: 40,
         margin:7,
         top: -10,
         backgroundColor: '#FFFFFF',
         height:40,
         borderColor:'gray',
         borderWidth:1
     },
     buttondaftar:{
        width: 140,
        height: 40,
        margin:15,
        backgroundColor: '#003366',
        borderRadius:16,
        alignItems:'center',
        alignContent:'center',
        left: 120,
                   
    },
    buttontext:{
        margin:1,
        justifyContent:'center',
        fontSize: 24,
        alignItems:'center',
        color:'#FFFFFF'
      
    },
    ataucss:{
        width: 294,
        height: 48,
        left: 170,
        margin:4,
        color:'#3EC6FF',
        fontSize:30
    },
    buttonmasuk:{
        width: 140,
        height: 40,
        margin:15,
        backgroundColor: '#3EC6FF',
        borderRadius:16,
        alignItems:'center',
        alignContent:'center',
        left: 120,
                   
    },
    

  

});