console.log("=================1. Mengubah fungsi menjadi fungsi arrow");
 var golden = () => {
    console.log("this is golden!!");
    return () => {
    }
} 
golden()
console.log("=================2. Sederhanakan menjadi Object literal di ES6");
  const newFunction = function literal(firstName, lastName){
    return {
       firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
console.log("=================3. Destructuring");  
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const { firstName, lastName,destination, occupation,spell} = newObject;

  console.log(firstName, lastName, destination, occupation)
console.log("================4. Array Spreading");  
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log("================5. Template Literals");  
const planet = "earth"
const view = "glass"
var before = `Lorem  ${planet}  dolor sit amet, 
    consectetur adipiscing elit, ${view}  do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
// Driver Code
console.log(before)


