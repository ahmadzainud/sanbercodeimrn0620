function range(startNum, finishNum){
    var numbers = [];
    var par1 = 0;
    var par2 = 0;
    if(startNum ==null || finishNum ==null){
        return -1;
    }
    if(startNum >finishNum){
        par1=finishNum;
        par2=startNum;
    }else{
        par1=startNum;
        par2=finishNum;
    }
    
    for(var a = 0; (par1+a) <= par2; a++) {
       numbers.push(par1+a);
    } 

    if(startNum >finishNum){
        numbers.sort(function(a, b){return b-a});
  }

return numbers;
}
console.log("=================Soal No. 1 (Range)=============");
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

function rangeWithStep(startNum, finishNum, step) {
    var numbers = [];
    var numbers2 = [];
    var par1 = 0;
    var par2 = 0;
    if(startNum ==null || finishNum ==null){
        return -1;
    }
    if(startNum >finishNum){
        par1=finishNum;
        par2=startNum;
    }else{
        par1=startNum;
        par2=finishNum;
    }
    
    for(var a = 0; (par1+a) <= par2; a++) {
        numbers.push(par1+a);   
    } 
    
    var g=[];
    if(startNum > finishNum){
        numbers.sort(function(a, b){return b-a});
   }
  
    for(var a = 0; a < numbers.length; a++) {
        if(a%step==0){
            numbers2.push(numbers[a]);
        }
       
    }

    return numbers2;
}

console.log("=================Soal No. 2 (Range with Step)=============")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

function sum(startNum, finishNum, step) {
    var numbers = [];
    if (startNum!=null && finishNum!=null && step!=null){
        numbers=rangeWithStep(startNum,finishNum,step);
    }else if (startNum!=null && finishNum!=null){
        numbers=range(startNum,finishNum);
    }else if (startNum!=null){
        numbers.push(startNum);
    }else{
        numbers=0;
    }
    var jum=0;
    for(var a = 0; a < numbers.length; a++) {
        jum=jum+numbers[a];
       
    }
    return jum;
}

console.log("=================Soal No. 3 (Sum of Range)=============")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(dataarr) {
    for(var a = 0; a < dataarr.length; a++) {
        console.log("Nomor ID: ",dataarr[a][0]);
        console.log("Nama Lengkap:",dataarr[a][1]);
        console.log("TTL : ",dataarr[a][2]," ",dataarr[a][3]);
        console.log("Hobi :",dataarr[a][4]);
        console.log();
       
    }
}
console.log("=================Soal No. 4 (Array Multidimensi)=============")
dataHandling(input);

function balikKata(data) {
    var hasil=""
    var jum=data.length
    for(var a = 0; a < data.length; a++) {
        jum=jum-1;
        hasil=hasil+data.charAt(jum);
     }
    return hasil;
}

console.log("=================Soal No. 5 (Balik Kata)=============");
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

function dataHandling2(dataarr) {
    dataarr.splice(1, 1, dataarr[1]+" Elsharawy");
    dataarr.splice(2, 1, "Provinsi "+dataarr[2]);
    dataarr.splice(4, 2, "Pria","SMA Internasional Metro");
    console.log(dataarr);
    var tgl =(dataarr[3]).split('/')
    switch(tgl[1]) {
        case '01':   {
             console.log(' Januari '); 
             break; 
            }
        case '02':   {
            console.log(' Febuari '); 
            break; 
            }
        case '03':   {
            console.log(' Maret '); 
            break; 
            }
        case '04':   {
            console.log(' April '); 
            break; 
            }
        case '05':   {
            console.log(' Mei '); 
            break; 
            }
        case '06':   {
            console.log(' Juni '); 
            break; 
            }
        case '07':   {
            console.log(' Juli '); 
            break; 
            }
        case '08':   {
            console.log(' Agustus '); 
            break; 
            }
        case '09':   {
            console.log(' September '); 
            break; 
            }
        case '10':   {
            console.log(' Oktober '); 
            break; 
            }
        case '11':   {
            console.log('  Novenber '); 
            break; 
            }
        case '12':   {
            console.log('  Desember '); 
            break; 
            }
    
        }
        var tgl2 = tgl.join("-")
        tgl.sort(function(a, b){return b-a});
        console.log(tgl); 
        console.log(tgl2); 
        var irisan1 = (dataarr[1]).slice(0,14) 
        console.log(irisan1); 
    return dataarr;
}
//["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("=================Soal No. 6 (Metode Array)=============");
dataHandling2(input)


