/* 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
class Score {
    constructor(par1,par2,par3) {
        this.subject = par1;
        this.points = par2;
        this.email = par3;
      }
      hasil() {
        for(let a = 0; a < this.subject.length; a++) {
          console.log(" subject : ",this.subject[a], "points : ",this.points[a]," email : ",this.email[a]);
          
        }
        console.log(" nilai rata2  : ",menghitungnilairata2(this.points));
      }
    }
function menghitungnilairata2(dataarr) {
  let nilairata=0;
  for(let a = 0; a < dataarr.length; a++) {
    nilairata=nilairata+dataarr[a];
  }
return (nilairata/dataarr.length);
}

const input = [
  ["ahmad", "zainud", "dahlan", "alan", "zainud"],
  [1, 2, 3, 2, 2],
  ["ahmad@gmail.com", "zainud@gmail.com", "dahlan@gmail.com", "alan@gmail.com", "zainud@gmail.com"]
] 

isi = new Score(input[0],input[1],input[2]);
isi.hasil();

/* 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]


function viewScores(data, subject) {
  let soal2arr=[]
  for(let a = 0; a < data.length; a++) {
    //console.log(" email : ",data[a][0], "subject : ",," points : ",);
    if(a>=1){

      if(subject=='quiz-1'){
        let studentName = {
          email: data[a][0],
          subject: data[0][1],
          points: data[a][1]
        };
        console.log(studentName);
      }else if(subject=='quiz-2'){
        let studentName2 = {
          email: data[a][0],
          subject: data[0][2],
          points: data[a][2]
      };
      console.log(studentName2);
      }else if(subject=='quiz-3'){
        let studentName3= {
          email: data[a][0],
          subject: data[0][3],
          points: data[a][3]
       };
     console.log(studentName3);
      }else{
        console.log("subject Tidak ada");
      }

    
    }
  }
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/* 
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  recapScore
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  for(let a = 0; a < data.length; a++) {
    if(a>=1){
      console.log(a,' Email:',data[a][0]);
      let nilainumbers = [];
      nilainumbers.push(data[a][1],data[a][2],data[a][3]);
      let nilairat=menghitungnilairata2(nilainumbers);
      console.log('   Rata-rata:',nilairat);
      if(nilairat>90){
        console.log('   Predikat: honour');
      }else if (nilairat>80){
        console.log('   Predikat: graduate');
      }else{
        console.log('   Predikat: participant ');
      }
    }
  }

}

recapScores(data);

    
  