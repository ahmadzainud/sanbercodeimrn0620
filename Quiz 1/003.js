/*

  A. Papan Ular Tangga (35)
    Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil akan menampilkan papan ular tangga ukuran 10 x 10. 

    Output: 
    100 99 98 97 96 95 94 93 92 91
    81 82 83 84 85 86 87 88 89 90
    80 79 78 77 76 75 74 73 72 71
    61 62 63 64 65 66 67 68 69 70
    60 59 58 57 56 55 54 53 52 51
    41 42 43 44 45 46 47 48 49 50
    40 39 38 37 36 35 34 33 32 31
    21 22 23 24 25 26 27 28 29 30
    20 19 18 17 16 15 14 13 12 11
    1 2 3 4 5 6 7 8 9 10

*/

function ularTangga() {
    // Tulis code kamu di sini
    var arr1 = [];
    var data=100;
    var hasil="";
    var kembalian=""
    for(var a = 0; a < 10; a++) {
        hasil="";
        arr1=[];
       // hasil=hasil+" "+data;
       // console.log("loop ");
        for(var b = 0; b < 10; b++) {
            arr1.push(data);
            if(a%2==0){
                arr1.sort(function(a, b){return b-a});
            }else{
                arr1.sort(function(a, b){return a-b});
            }
           data--;
        }
        hasil="";
        for(var c = 0; c < arr1.length; c++) {
            hasil=hasil+" "+arr1[c];
                 
        }
        kembalian=kembalian+hasil+"\n";
       // console.log(hasil);
    }
    return kembalian;
  }
  
  // TEST CASE Ular Tangga
  //ularTangga();
  console.log(ularTangga())
  /*
  Output :
    100 99 98 97 96 95 94 93 92 91
    81 82 83 84 85 86 87 88 89 90
    80 79 78 77 76 75 74 73 72 71
    61 62 63 64 65 66 67 68 69 70
    60 59 58 57 56 55 54 53 52 51
    41 42 43 44 45 46 47 48 49 50
    40 39 38 37 36 35 34 33 32 31
    21 22 23 24 25 26 27 28 29 30
    20 19 18 17 16 15 14 13 12 11
    1 2 3 4 5 6 7 8 9 10
  */