function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear();
    if(arr.length<=0){
        console.log("Data Yang Dikirim Kosong");
    }else{
        for(var a = 0; arr.length > a; a++) {
            var umur=0;
            if(arr[a][3]!=null){
                umur=thisYear- (arr[a][3]);
                if(umur<0){
                    umur= "Invalid birth year";
                }
            }else{
                umur= "Invalid birth year";
            }
    
             var personObj = {
                firstName : arr[a][0],
                lastName: arr[a][1],
                gender: arr[a][2],
                age:umur
               
                
            } 
                
            console.log((a+1),". ",personObj.firstName," ",personObj.lastName," ",personObj);
        } 

    }
   
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);
arrayToObject([]);

function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(memberId=="" || memberId == null){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }
    if(money < 50000){
        return 'Mohon maaf, uang tidak cukup';
    }
    var lisejual = [ ["Sepatu Stacattu", 1500000],
                     ["Baju Zoro", 500000],
                     ["Baju H&N", 250000],
                     ["Sweater Uniklooh", 175000],
                     ["Casing Handphone", 50000] ];
    var lisekeluar=[];
    var kembalian=0;
    for(var a = 0; lisejual.length > a; a++) {
        if((lisejual[a][1]) <= money){
            lisekeluar.push(lisejual[a][0]);
            kembalian=money-(lisejual[a][1]);
        }
    }
    if(lisekeluar.length>1){
        kembalian=0;
    }
    
    var personObj = {
        memberId : memberId,
        money: money,
        listPurchased: lisekeluar,
        changeMoney:kembalian
              
    } 
    
return personObj;
  }

// console.log(shoppingTime('1820RzKrnWn08', 2475000));
// console.log(shoppingTime('82Ku8Ma742', 170000));
// console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
// console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
// console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function cek(a) {
    var b=0;
    switch(a) {
        case 'A':   {
            b=0;
             break; 
            }
        case 'B':   {
            b=1;
            break; 
            }
        case 'C':   {
            b=2;
            break; 
            }
        case 'D':   {
            b=3;
            break; 
            }
        case 'E':   {
            b=4;
            break; 
            }
        case 'F':   {
            b=5;
            break; 
            }
          
        }
        return b;
}

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var penumpangarr=[];
    for(var a = 0; arrPenumpang.length > a; a++) {
        var banyk=cek(arrPenumpang[a][2])-cek(arrPenumpang[a][1]);
        var personObj = {
            penumpang : arrPenumpang[a][0],
            naikDari: arrPenumpang[a][1],
            tujuan: arrPenumpang[a][2],
            bayar:(banyk*2000)
                  
        } 
        penumpangarr.push(personObj);
    }
    return penumpangarr;
  }
//   console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
//   console.log(naikAngkot([])); //[]